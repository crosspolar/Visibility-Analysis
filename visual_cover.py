from os.path import exists

import geopandas
import pandas as pd
from tqdm import tqdm

from file_handler import FileHandler

tqdm.pandas()


def gebaudehoehe(aog):
    """
    assumes each floor is about 4 meter high + 2 meters of roof
    :param aog: number of floors
    :return: height in meter
    """
    h = aog * 4.0 + 2.0
    if h > 1000.0:
        return -9999
    else:
        return h


class VisualCover(FileHandler):

    def visual_cover_layer(self, **kwargs):
        # tqdm.pandas()
        if "epsg_crs" in kwargs:
            epsg_crs = kwargs.get("epsg_crs")
        else:
            epsg_crs = self._defaultEPSG
        vegetation = self.__vegetation_file__(self.bbox, epsg_crs, **kwargs)
        vegetation_boundary = vegetation.set_geometry(vegetation.boundary)
        # bbox = tuple(transform_point(self.__bbox, 'EPSG:25832').bounds)
        buildings = self.__gebaeude_file__(epsg_crs=epsg_crs, **kwargs)
        buildings_boundary = buildings.set_geometry(buildings.boundary)
        df = pd.concat([buildings_boundary, vegetation_boundary]).reset_index(drop=True)
        if "contour" in kwargs:
            contour = kwargs.get("contour")
            contour["type"] = "surface"
            contour["abs_hoehe"] = contour["elev"]
            df = pd.concat([df, contour]).reset_index(drop=True)
        return df

    def __vegetation_file__(self, boundarybox=None, epsg_crs=None, **kwargs):
        """
        CRS: Assumes inbound boundaryBox is 4326 and Shapefile also

        :param boundarybox: Boundary box (EPSG:4326)
        :param epsg_crs: The Output CRS
        :return:
        """
        path_to_proc = "data/export/vegetation.geojson"
        if not exists(path_to_proc):
            print("Process veg:")
            path_to_file = "data/berlin-latest-free/gis_osm_landuse_a_free_1.shp"  # EPSG:4326
            if not exists(path_to_file):
                print("Download Vegetation")
                self.download_and_unzip("https://download.geofabrik.de/europe/germany/berlin-latest-free.shp.zip",
                                        "data/berlin"
                                        "-latest-free")
            if boundarybox is not None:
                v = geopandas.read_file(path_to_file, bbox=boundarybox)
            else:
                v = geopandas.read_file(path_to_file)
            inp_list = ['forest', 'cemetery', 'allotments']
            v = v[v.fclass.isin(inp_list)]
            if 'hoehe' not in v.columns:
                v['hoehe'] = 17
            if 'type' not in v.columns:
                v['type'] = "vegetation"
            if 'abs_hoehe' not in v.columns and "dem" in kwargs:
                print("add absolute height to vegetation")
                dem = kwargs.get("dem")
                v = v.to_crs(epsg_crs)
                v["abs_hoehe"] = v.apply(
                    lambda row: dem.sample_point(row.geometry.centroid) + row.hoehe,
                    axis=1
                )
            else:
                print("Wasn't able to calculate absolute heights for vegetation")
            v.to_file(path_to_proc, driver="GeoJSON")
        else:
            print("vegetation file found --> open")
            v = geopandas.read_file(path_to_proc)
        if epsg_crs is not None:
            v = v.to_crs(epsg_crs)
        return v

    def __gebaeude_file__(self, boundarybox=None, epsg_crs=None, **kwargs):
        path_to_proc = "data/export/gebaeude.geojson"
        path_to_file = "data/gebaeude.shp"
        if not exists(path_to_proc):
            print("Process bldng:")
            if not exists(path_to_file):
                print("Download Gebäude")
                self.download_and_unzip("https://tsb-opendata.s3.eu-central-1.amazonaws.com/gebaeude/gebaeude.shp.zip")
            bldngs = geopandas.read_file(path_to_file)
            if 'hoehe' not in bldngs.columns:
                print("height of bldng")
                bldngs['hoehe'] = bldngs['aog'].progress_apply(lambda aog: gebaudehoehe(aog))
            bldngs = bldngs[bldngs.ofl != 1200]  # exclude basements
            if 'type' not in bldngs.columns:
                bldngs['type'] = "building"
            if 'abs_hoehe' not in bldngs.columns and "dem" in kwargs:
                print("Absolute height pf bldng")
                bldngs = bldngs.to_crs(4326)
                dem = kwargs.get("dem")
                a = bldngs['geometry'].progress_apply(
                    lambda geo: dem.sample_point(geo.centroid, point_crs="EPSG:4326")
                )
                bldngs["abs_hoehe"] = a.fillna(0) + bldngs.hoehe.fillna(0)
            else:
                print("Wasn't able to calculate absolute heights for buildings")
            bldngs.to_file(path_to_proc, driver="GeoJSON")
        else:
            bldngs = geopandas.read_file(path_to_proc)
        if epsg_crs is not None:
            bldngs = bldngs.to_crs(epsg_crs)
        return bldngs
