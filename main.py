import math
from os.path import exists

import fiona
import geopandas
import numpy as np
import shapely
from shapely.geometry import shape, LineString
from shapely.ops import unary_union, nearest_points

from dem_helper import Dem
from visual_cover import VisualCover


def equidistant_points(delta, line):
    """
    Sets points along a line with same distance among each other
    https://stackoverflow.com/questions/62990029/how-to-get-equally-spaced-points-on-a-line-in-shapely

    :param delta: distance between points[projection unit]
    :param line: shapely line the points sample along
    :return: shapely MultiPoint
    """
    distances = np.arange(0, line.length, delta)
    pts = [line.interpolate(distances) for distances in distances] + [line.boundary[1]]
    multipoint = unary_union(pts)
    return multipoint


def viewing_angle(distance_to_obstacle, building_height=22):
    """
    The function assumes planar ground.

    :param distance_to_obstacle: Distance from observer to the first obstacle, i.e. building (meter)
    :param building_height: Height of that building above observer level (meter). Default "Berliner Traufhöhe" (22 m)
    :return: the angle alpha from the observer to the building top (radian)
    """
    # trigonometry
    a = math.atan2(building_height, distance_to_obstacle)
    return a


def height_on_fernsehturm(verticalAngle, distance_to_fernsehturm):
    """
    https://stimming-sh.de/schule/mathematik/winkelfunktionen/
    Calculates the height of the lowest point which can be seen non-blocked by the obstacle.

    :param verticalAngle: the angle alpha from the observer to the building top (radian)
    :param distance_to_fernsehturm: the distance between observer and Fernsehturm (m)
    :return: the lowest point which can be seen over the building (m)
    """
    h_f = math.tan(verticalAngle) * distance_to_fernsehturm
    h_f = h_f + distance_to_fernsehturm ** 2 / (2 * 6371000)  # Spherical Earth
    return h_f


def nearest_point(geo, observerPoint):
    """
    If intersection() returns MultiPoint, nearest_point() returns the one point nearest to observer

    :param geo: a 'geometry' field
    :param observerPoint: shapely point or MultiPoint of observer
    :return: shapely Point of nearest point
    """
    poi = geo.intersection(sichtlinie)
    if poi.type == "MultiPoint":
        poi = nearest_points(poi, observerPoint)[0]
    return poi


strassen = fiona.open("/home/frederik/Dokumente/GIS/Data/Berlin Straßen "
                      "2021/Detailnetz-Strassenabschnitte.shp")
strassen_gs = geopandas.GeoSeries([shape(line['geometry']) for line in strassen], crs=strassen.crs['init'])
bbox = tuple(strassen_gs.to_crs(4326).total_bounds)  # (minx, miny, maxx, maxy)
vc = VisualCover(bbox=bbox)
dem = Dem(boundaryBox=bbox)
contours = dem.contours()
block_ls_path = "data/export/block_ls.geojson"
if exists(block_ls_path):
    block_ls = geopandas.read_file(block_ls_path)
else:
    block_ls = vc.visual_cover_layer(contour=contours, dem=dem)
    block_ls.to_file(block_ls_path, driver="GeoJSON")
block_ls_sindex = block_ls.sindex  # build spatial index


strassenpoints = [equidistant_points(50, shape(line['geometry'])) for line in strassen]  # Punkte alle paar Meter
fernsehturm = shapely.geometry.Point(392083.14, 5820154.81)  # epsg:25833

d = []
i_start = 0
export_filepath = "data/export/g_df.geojson"
if exists(export_filepath):
    prev_g_df = geopandas.read_file(export_filepath)
    prev_g_df.apply(lambda row: d.append(row.to_dict()), axis=1)
    i_start = max(prev_g_df.strassen_nr) + 1
    print(f"set start to {i_start}")

# strassenpoints = strassenpoints[0:200]

length_of_strassenpoints = len(strassenpoints)

for i in range(i_start, length_of_strassenpoints):
    # all viewing points are grouped by street
    points = strassenpoints[i]
    strasse_id = i
    strassen_name = strassen[i]['properties']['strassenna']
    print(f"{i}/{length_of_strassenpoints} with {len(points)}")
    d_tmp = []
    for j in range(len(points)):
        #
        point = points[j]
        punkt_id = j
        sichtlinie = LineString([point, fernsehturm])
        # samplePoint_buffered = point.buffer(500)
        point_altitude = dem.sample_point(point)
        buildings_along_sichtlinie = block_ls.iloc[list(block_ls_sindex.query(sichtlinie))]

        intersectionpoints_along_sichtlinie = buildings_along_sichtlinie.copy()
        intersectionpoints_along_sichtlinie.geometry = buildings_along_sichtlinie.geometry.apply(
            lambda geo: nearest_point(geo, point)
        )
        intersectionpoints_along_sichtlinie = intersectionpoints_along_sichtlinie[
            ~intersectionpoints_along_sichtlinie.geometry.is_empty]  # delete empty geometries
        # exclude geometries too close to Fernsehturm, as they could considered be the Fernsehturm itself
        intersectionpoints_along_sichtlinie = intersectionpoints_along_sichtlinie[
            ~intersectionpoints_along_sichtlinie.geometry.within(fernsehturm.buffer(25))]
        intersectionpoints_along_sichtlinie["dist"] = intersectionpoints_along_sichtlinie.geometry.apply(
            lambda geo: point.distance(geo)
        )
        # print("alpha")
        intersectionpoints_along_sichtlinie["alph"] = intersectionpoints_along_sichtlinie.apply(
            lambda row: viewing_angle(distance_to_obstacle=row.dist, building_height=row.abs_hoehe-point_altitude),
            axis=1
        )
        # print("height on fernsehturm")
        intersectionpoints_along_sichtlinie["height_on_fernsehturm"] = intersectionpoints_along_sichtlinie.alph.apply(
            lambda alph: height_on_fernsehturm(verticalAngle=alph, distance_to_fernsehturm=sichtlinie.length)
        )
        # find highest point on Fernsehturm
        intersectionpoints_along_sichtlinie = intersectionpoints_along_sichtlinie.reset_index()
        hgst = intersectionpoints_along_sichtlinie.iloc[
               intersectionpoints_along_sichtlinie[["height_on_fernsehturm"]].idxmax(), :]
        hgst = hgst.reset_index()
        d_tmp.append({
            'geometry': point,
            'point_altitude': point_altitude,
            # 'highest_obstacle': hgst.loc[0, 'geometry'],
            'obstacle_height': hgst.loc[0, 'hoehe'],
            'obstacle_type': hgst.loc[0, 'type'],
            'dist': hgst.loc[0, 'dist'],
            'viewing_angle': hgst.loc[0, 'alph'],
            'height_on_fernsehturm': hgst.loc[0, 'height_on_fernsehturm'],
            'strassenname': strassen_name,
            'strassen_nr': i,
            'strassen_punkt_nr': j
        })
    [d.append(d_tmp_i) for d_tmp_i in d_tmp]
    if i % 20 == 0:
        g_df = geopandas.GeoDataFrame(d, crs='epsg:25833')
        g_df.to_file(export_filepath, driver="GeoJSON")

g_df = geopandas.GeoDataFrame(d, crs='epsg:25833')
g_df.to_file(export_filepath, driver="GeoJSON")
