import io
import zipfile

import pyproj
import requests
# from shapely.geometry import box
from shapely.ops import transform


class FileHandler:

    def __init__(self, bbox=None):
        self._bbox = bbox
        self._defaultEPSG = 25833  # for Berlin

    @property
    def bbox(self):
        """
        (minx, miny, maxx, maxy)

        :return: tuple
        """
        return self._bbox

    @bbox.setter
    def bbox(self, bbox):
        """
        receives four float values from Boundary Box
        TODO check if is tuple

        :param bbox:
        :return:
        """
        if bbox is not None:
            # self.__bbox = box(bbox[0], bbox[1], bbox[2], bbox[3])
            self._bbox = bbox

    @staticmethod
    def download_and_unzip(url, path="data"):
        r = requests.get(url)
        z = zipfile.ZipFile(io.BytesIO(r.content))
        z.extractall(path)

    @staticmethod
    def transform_point(point, to_crs, from_crs='EPSG:4326'):
        """

        :param point:
        :param from_crs:
        :param to_crs:
        :return:
        """
        wgs84 = pyproj.CRS(from_crs)
        utm = pyproj.CRS(to_crs)
        project = pyproj.Transformer.from_crs(wgs84, utm, always_xy=True).transform
        transformed_point = transform(project, point)
        return transformed_point
