from os.path import exists

import geopandas
import rasterio
from rasterio.mask import mask
from rasterio.plot import show
from shapely.geometry import box
from osgeo import gdal, osr, ogr

from file_handler import FileHandler


class Dem(FileHandler):

    def __init__(self, boundaryBox=None):
        super().__init__()
        self.__path_to_sourcefile__ = "data/srtm_germany_dtm.tif"
        self.__path_to_clipfile__ = "data/clip.tif"
        self.__path_to_contourfile__ = "data/contour.shp"
        # https://www.digitalocean.com/community/tutorials/understanding-class-inheritance-in-python-3
        self.bbox = boundaryBox
        self.url = "https://www.opendem.info/downloads/srtm_germany_dtm.zip"
        self.__load__()
        # self.contours()

    def __load__(self):

        if not exists(self.__path_to_sourcefile__):
            print("Download DEM")
            self.download_and_unzip(self.url)
        if exists(self.__path_to_clipfile__):
            self.data = rasterio.open(self.__path_to_clipfile__, "r")
        if not exists(self.__path_to_clipfile__) and self.bbox is not None:
            print("Clip DEM")
            bbox = self.bbox
            with rasterio.open(self.__path_to_sourcefile__) as src:
                out_image, out_transform = mask(src, [box(bbox[0], bbox[1], bbox[2], bbox[3])], crop=True)
                out_meta = src.meta
            out_meta.update({"driver": "GTiff",
                             "height": out_image.shape[1],
                             "width": out_image.shape[2],
                             "transform": out_transform})

            with rasterio.open(self.__path_to_clipfile__, "w", **out_meta) as dest:
                dest.write(out_image)
            self.data = rasterio.open(self.__path_to_clipfile__, "r")
        else:
            self.data = rasterio.open(self.__path_to_sourcefile__, "r")
        # self.data = gdal.Open(__path_to_clipfile__)
        # https://www.earthdatascience.org/tutorials/visualize-digital-elevation-model-contours-matplotlib/

    def sample_point(self, point, point_crs='epsg:25833'):
        to_crs = 'EPSG:4326'
        if point_crs != to_crs:
            point = self.transform_point(point, from_crs=point_crs, to_crs=to_crs)
        x = point.xy[0][0]
        y = point.xy[1][0]
        row, col = self.data.index(x, y)
        # print("Point correspond to row, col: %d, %d" % (row, col))
        # print("Raster value on point %.2f \n" % self.data.read(1)[row, col])
        return self.data.read(1)[row, col]

    def contours(self, epsg_crs=None):
        if exists(self.__path_to_clipfile__):
            indataset1 = gdal.Open(self.__path_to_clipfile__)
        rasterBand = indataset1.GetRasterBand(1)
        ogr_ds = ogr.GetDriverByName("ESRI Shapefile").CreateDataSource(self.__path_to_contourfile__)
        sr = osr.SpatialReference(indataset1.GetProjection())
        contour_shp = ogr_ds.CreateLayer('contour', sr)

        # define fields of id and elev
        fieldDef = ogr.FieldDefn("ID", ogr.OFTInteger)
        contour_shp.CreateField(fieldDef)
        fieldDef = ogr.FieldDefn("elev", ogr.OFTReal)
        contour_shp.CreateField(fieldDef)

        gdal.ContourGenerate(
            rasterBand,  # Band srcBand
            10,  # double contourInterval - This defines contour intervals
            0,  # double contourBase
            [],  # int fixedLevelCount
            0,  # int useNoData
            0,  # double noDataValue
            contour_shp,  # Layer dstLayer
            0,  # int idField
            1  # int elevField
        )
        # ogr_ds.Destroy() - Better not to use this method. See here: https://gdal.org/api/python_gotchas.html
        # instead, empty and delete the dataframe
        ogr_ds = None
        del ogr_ds
        contourDf = geopandas.read_file(self.__path_to_contourfile__)
        if epsg_crs is not None:
            contourDf = contourDf.to_crs(epsg_crs)
        else:
            contourDf = contourDf.to_crs(self._defaultEPSG)
        return contourDf

    def sample_line(self, line, line_crs='epsg:25833'):
        """
        creates points at raster cell edges with cell values as attribute

        :param line: shapely line
        :param line_crs: coordinate system of line
        :return: geodataframe with points and their respective sampled cell values
        """

    def plot(self):
        show(self.data)
